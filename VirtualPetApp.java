import java.util.Scanner;
public class VirtualPetApp{
	public static void main(String[] args){
		
		Scanner reader = new Scanner (System.in);
		Monkey[] baboons = new Monkey[4];
		for(int i=0; i < baboons.length; i++){

			baboons[i] = new Monkey();
			System.out.println("What breed is your Baboon?");
			baboons[i].breed = reader.nextLine();
			System.out.println("What is your Baboons current mood?");
			baboons[i].currentMood = reader.nextLine();
			System.out.println("How old is your Baboon?");
			baboons[i].age = Integer.parseInt(reader.nextLine());
		}
		System.out.println(baboons[baboons.length -1].breed);
		System.out.println(baboons[baboons.length -1].currentMood);
		System.out.println(baboons[baboons.length -1].age);
		
		baboons[0].eatBananas();
		baboons[0].sayAge();
	}
}